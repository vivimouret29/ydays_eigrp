# YDAYS EIGRP

EIGRP, pour *Enhanced Interior Gateway Routing Protocol*, est un protocole développé par **CISCO** que nous allons utilisé ici.

## Arborescence  
  
  
![eigrp](proto-eigrp/capture_eigrp.png)


## Capture du système

Configuration du routeur A, identique (sauf les IP) aux autres routeurs :

```bash
A#sh ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            10.10.10.25     YES NVRAM  up                    up      
FastEthernet1/0            10.10.10.1      YES NVRAM  up                    up      
FastEthernet2/0            10.10.10.9      YES NVRAM  up                    up      
FastEthernet3/0            10.10.10.21     YES NVRAM  up                    up      
Loopback0                  1.1.1.1         YES NVRAM  up                    up 


A#sh ip route

Gateway of last resort is not set

     1.0.0.0/8 is variably subnetted, 2 subnets, 2 masks
C       1.1.1.1/32 is directly connected, Loopback0
D       1.0.0.0/8 is a summary, 00:14:02, Null0
     10.0.0.0/8 is variably subnetted, 8 subnets, 2 masks
C       10.10.10.8/30 is directly connected, FastEthernet2/0
D       10.10.10.12/30 [90/30720] via 10.10.10.22, 00:14:02, FastEthernet3/0
                       [90/30720] via 10.10.10.2, 00:14:02, FastEthernet1/0
C       10.10.10.0/30 is directly connected, FastEthernet1/0
D       10.0.0.0/8 is a summary, 00:14:16, Null0
D       10.10.10.4/30 [90/30720] via 10.10.10.22, 00:14:05, FastEthernet3/0
                      [90/30720] via 10.10.10.10, 00:14:05, FastEthernet2/0
C       10.10.10.24/30 is directly connected, FastEthernet0/0
D       10.10.10.16/30 [90/30720] via 10.10.10.10, 00:14:05, FastEthernet2/0
                       [90/30720] via 10.10.10.2, 00:14:05, FastEthernet1/0
C       10.10.10.20/30 is directly connected, FastEthernet3/0
```
Voisin direct de A :

```bash
A#sh ip eigrp nei
IP-EIGRP neighbors for process 1
H   Address                 Interface       Hold Uptime   SRTT   RTO  Q  Seq
                                            (sec)         (ms)       Cnt Num
2   10.10.10.10             Fa2/0             11 00:02:36   35   210  0  26
1   10.10.10.2              Fa1/0             10 00:02:36   31   200  0  23
0   10.10.10.22             Fa3/0              6 00:02:36   44   264  0  23
```

## Utilisation du protocole EIGRP

Ce protocole permet de résoudre des problèmes des protocoles de routage.
Il peut se configurer en connaissant donc ces voisins direct et des voisins indirects !
Il prend pour cela en compte la bande passante et le délai.
Il connait le chemin le plus court et un chemin dit de secours afin d'avoir un chemin comme lors de pannes.

## Test

Vérifions si la machine E arrive à pinguer C :

```bash 
E#ping 10.10.10.10

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 10.10.10.10, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 64/69/76 ms
```